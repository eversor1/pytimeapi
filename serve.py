import falcon
import json
import datetime
from time import strftime, gmtime

class GetDatTime:
  def on_get(self, req, resp):
    #time_stamp = strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())
    time_stamp = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
  
    time_resp = {
      'Name': "Time",
      'Value': '{}'.format(time_stamp)
    }

    resp.body = json.dumps(time_resp)

api = falcon.API()
api.add_route('/time', GetDatTime())
